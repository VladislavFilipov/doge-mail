import { getLettersFromServer } from '../api';

export const SELECT_LETTER = 'SELECT_LETTER';
export function selectLetter(letterId) {
    return {
        type: SELECT_LETTER,
        letterId
    }
};

export const GET_LETTERS = 'GET_LETTERS';
export function getLetters(letters) {
    return {
        type: GET_LETTERS,
        letters
    }
};


export function getLettersThunk() {
    return dispatch => {
        getLettersFromServer()
            .then((letters) => {
                dispatch(getLetters(letters));
            });
    };
};

