import { combineReducers } from 'redux';
import letters from './letters';
import selectedLetter from './selectedLetter';

const rootReaducer = combineReducers({
    letters,
    selectedLetter
});

export default rootReaducer;