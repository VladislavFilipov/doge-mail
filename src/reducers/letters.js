import { GET_LETTERS } from '../actions';

export default function letters(state = null, action) {
    switch (action.type) {
        case GET_LETTERS:
            return action.letters;

        default:
            return state;
    }
}