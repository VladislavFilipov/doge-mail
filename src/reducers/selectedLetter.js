import { SELECT_LETTER } from '../actions';

export default function selectedLetter(state = null, action) {
    switch (action.type) {
        case SELECT_LETTER:
            return action.letterId;

        default:
            return state;
    }
}