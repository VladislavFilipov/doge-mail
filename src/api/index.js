import axios from 'axios';

export function getLettersFromServer() {
    return axios.post('http://localhost:1488/get-letters', { login: process.env.IMAP_LOGIN, psw: process.env.IMAP_PSW })
        .then(res => res.data);
};

export function sendMessage(message) {
    return axios.post('http://localhost:1488/send-message', { message, login: process.env.IMAP_LOGIN, psw: process.env.IMAP_PSW })
        .then(res => console.log(res));
};

export function deleteMessage(id) {
    return axios.post('http://localhost:1488/delete-message', { id, login: process.env.IMAP_LOGIN, psw: process.env.IMAP_PSW })
        .then(res => console.log(res));
};