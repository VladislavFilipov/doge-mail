import React from 'react';

import Header from '../Header/Header';
import LettersList from '../LettersList/LettersList';
import Letter from '../Letter/Letter';

import './App.scss';

function App() {
    return (
        <div className="App">
            <Header />
            <LettersList />
            <Letter />
        </div>
    );
}

export default App;
