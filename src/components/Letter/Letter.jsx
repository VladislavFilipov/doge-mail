import React from 'react';
import { useSelector } from 'react-redux';
import parse from 'html-react-parser';
import fileDownload from 'js-file-download';

import './Letter.scss';

import { deleteMessage } from '../../api';

function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
};

function Letter() {
    const letters = useSelector(state => state.letters);
    const selectedLetter = useSelector(state => state.selectedLetter);
    let letter = null;

    if (letters) {
        letter = letters.find(letter => letter.id === selectedLetter);
    };

    if (!letter) {
        return (
            <div className="Letter">
                <div className="Letter__wow">
                    such mail <br />
                    much letters <br />
                    wow
                </div>
            </div>
        );
    };

    let html = letter.body.find(item => item.type === 'html');

    return (
        <div className="Letter">
            <div className="Letter__delete" onClick={() => deleteMessage(letter.id)}>Удалить сообщение</div>
            <div className="Letter__content">
                <div className="Letter__from"><b>От кого:</b> {letter.from}</div>
                <div className="Letter__to"><b>Кому:</b> {letter.to}</div>
                <div className="Letter__date"><b>Дата и время:</b> {letter.date}</div>
                <div className="Letter__subject"><b>Заголовок:</b> {letter.subject}</div>
                <hr />
                <ul className="Letter__attachments">
                    {
                        letter.attachments.map((attachment, i) => (
                            <li key={i} onClick={() => {
                                var sampleArr = base64ToArrayBuffer(attachment.data);
                                let blob = new Blob([sampleArr]);
                                fileDownload(blob, attachment.filename);
                            }}>{attachment.filename}</li>
                        ))
                    }
                </ul>
                <div className="Letter__body">{parse(html.data)}</div>
            </div>
        </div>
    );
}

export default Letter;
