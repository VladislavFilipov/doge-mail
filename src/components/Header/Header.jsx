import React from 'react';
import './Header.scss';
import { useState } from 'react';
import WriteMsg from '../WriteMsg/WriteMsg';

function Header() {

    let [showWriteMsg, setShowWriteMsg] = useState(false);

    return (
        <div className="Header">
            <div className="Header__filters">
            </div>
            <div className="Header__logo">
                <img src="doge.png" alt="doge" />
                <h1>DogeMail</h1>
            </div>
            <div className="Header__write-msg" onClick={() => setShowWriteMsg(true)}>Написать сообщение</div>
            {
                showWriteMsg && <WriteMsg closeForm={() => setShowWriteMsg(false)} />
            }
        </div>
    );
}

export default Header;
