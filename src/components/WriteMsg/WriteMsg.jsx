import React from 'react';

import './WriteMsg.scss';
import { useState } from 'react';

import { sendMessage } from '../../api';

function arrayBufferToBase64(buffer) {
    var binary = '';
    var bytes = new Uint8Array(buffer);
    var len = bytes.byteLength;
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}

function WriteMsg({ closeForm }) {

    let [to, setTo] = useState();
    let [subject, setSubject] = useState();
    let [text, setText] = useState();

    const sendMsg = e => {
        e.preventDefault();

        let blob = e.target.elements.files.files[0];

        if (blob) {
            let reader = new FileReader();
            let file;

            reader.onload = function () {
                // console.log(reader.result.toString('utf8'));
                file = arrayBufferToBase64(reader.result);
                // console.log(file);
                // e.target.querySelector('#file').innerText = reader.result;

                sendMessage({ to, subject, text, attachments: [{ filename: blob.name, content: file, encoding: 'base64' }] });
            };

            reader.readAsArrayBuffer(blob);
        }

        sendMessage({ to, subject, text });

        closeForm();
    }

    return (
        <div className="WriteMsg">
            <form onSubmit={sendMsg}>
                <label>
                    <div>Кому</div>
                    <input type="text" placeholder="Введите адрес получателя" name="to" value={to} onChange={e => setTo(e.currentTarget.value)} />
                </label>
                <label>
                    <div>Заголовок</div>
                    <input type="text" placeholder="Введите заголовок сообщения" name="subject" value={subject} onChange={e => setSubject(e.currentTarget.value)} />
                </label>
                <label>
                    <div>Текст сообщения</div>
                    <textarea type="text" placeholder="Введите текст сообщения" name="text" value={text} onChange={e => setText(e.currentTarget.value)} />
                </label>
                <label>
                    <div>Выбрать файл</div>
                    <input type="file" name="files" />
                    {/* <div id="file"></div> */}
                </label>
                <button className="submit" type="submit">Отправить</button>
                <div className="close" onClick={() => closeForm()}><div>+</div></div>
            </form>
        </div>
    );
}

export default WriteMsg;
