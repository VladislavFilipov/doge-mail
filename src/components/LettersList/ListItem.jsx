import React from 'react';
// import './ListItem.scss';

function parseAuthor(str) {
    const res = str.match(/(.+)\s<.+>/);
    if (!res) {
        return str;
    } else {
        if (res[1][0] === '"') return res[1].slice(1, -1);

        return res[1];
    }

}

function parseTime(str) {
    const res = new Date(Date.parse(str));
    // console.log(res);
    return `${res.getHours()}:${res.getMinutes() < 10 ? '0' + res.getMinutes() : res.getMinutes()}\n${res.getDate()}.${res.getMonth() === 11 ? 12 : res.getMonth() + 1} `;
}

function ListItem({ author, title, time }) {

    const parsedAuthor = parseAuthor(author[0]);

    return (
        <div className="ListItem">
            <div className="ListItem__avatar">{parsedAuthor[0].toUpperCase()}</div>
            <div className="ListItem__info">
                <div className="ListItem__author">{parsedAuthor}</div>
                <div className="ListItem__title">{title}</div>
            </div>
            <div className="ListItem__datetime">{parseTime(time)}</div>

        </div>
    );
}

export default ListItem;
