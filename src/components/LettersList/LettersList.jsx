import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { selectLetter, getLettersThunk } from '../../actions';

import './LettersList.scss';
import ListItem from './ListItem';
import { useEffect } from 'react';

function LettersList() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getLettersThunk());
    }, [dispatch]);

    const letters = useSelector(state => state.letters);

    if (!letters) {
        return (
            <div className="LettersList">
                Wait
            </div>
        )
    }

    return (
        <div className="LettersList">
            {letters.slice(10).reverse().map((letter, i) => (
                <div key={i} onClick={() => dispatch(selectLetter(letter.id))}>
                    <ListItem author={letter.from} title={letter.subject} time={letter.date} />
                </div>
            ))}
        </div>
    );
}

export default LettersList;
